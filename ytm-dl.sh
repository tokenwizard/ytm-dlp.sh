#!/bin/bash
### Usage: ytm-dl.sh <Album URL> <audio format (optional)>

## Notes: This script requires python. If yt-dlp is not 
## installed it will be installed on the first run

## Verify the Album URL was passed to the script
if [[ -z $1 ]] || [[ -z $2 ]]; then
	echo "Usage: ytm-dl.sh <Album URL> <audio format>"
	sleep 3
	exit
fi

## If the audio format was passed in the second parameter, check 
## to make sure it is a valid format and set the format variable
if [[ ${2} -ne 'mp3' ]] && [[ ${2} -ne 'opus' ]] && [[ ${2} -ne 'aac' ]] && [[ ${2} -ne 'flac' ]] && [[ ${2} -ne 'm4a' ]] && [[ ${2} -ne 'vorbis' ]] && [[ ${2} -ne 'wave' ]] && [[ ${2} -ne 'alac' ]]; then
	echo "audio format must be one of: aac, alac, flac, m4a, mp3, opus, vorbis, wav"; 
	sleep 1;
	exit
fi
## Set Album URL from 1st parameter
album_url="${1}"

## Set the Audio Format form the 2nd parameter
audio_fmt="${2}"

##Display Audio Format
echo "Audio Format: "$audio_fmt
sleep 1

## Activate python venv
cd ~
if [[ ! -d .config/yt-dlp-venv ]]; then 
	echo "Creating python venv in ~/.config/yt-dlp-venv";
	sleep 1;
	python -m venv ~/.config/yt-dlp-venv; 
fi
source ~/.config/yt-dlp-venv/bin/activate

## Check if yt-dlp is installed and install if necessary
pip show yt-dlp > /dev/null
if [[ $? -ne '0' ]]; then
	echo "yt-dlp is not installed in the venv. Installing now...";
	pip install yt-dlp;
fi

## Mention where the Download will go
echo ""
echo "Album will be downloaded to the ~/Music folder"
sleep 2

## Download the Album
echo ""
yt-dlp -x $album_url --audio-format $audio_fmt -o ~/Music/"%(channel)s - %(album)s/%(playlist_index)s - %(title)s.%(ext)s"


