# ytm-dlp.sh

This project is just a bash script wrapper for yt-dlp to simplify downloading albums from YT Music. All credit goes to those developers here: https://github.com/yt-dlp/yt-dlp

This script allows you to use the "share" link from YT Music to download albums. 

Simply browse YT Music to the Album you are interested in and click the three-dot menu. Select "Share" and copy the provided URL.
Paste that URL as the 1st paramter with the script along with the desired audio output format in the 2nd parameter:
`ytm-dlp.sh <share_url> <audio_format>`

Available audio formats include:

`best aac alac flac m4a mp3 opus vorbis wav`

On each run, the script will check to see if the python venv exists (~/.config/yt-dlp-venv) and create it if necessary. 
Next it will activate the venv and check to see if yt-dlp is installed, installing it if necessary. 
Lastly, once the venv is activated and yt-dlp is installed, it will download the requested Album to your ~/Music folder. 

The output path is formatted as such:
`~/Music/<channel> - <album>/<playlist_index> - <track_name>`

So, for example, let's look at Tom Morello's 2021 album titled "The Atlas Underground Fire" and see how this all works:

Let's run the script with the URL to the album as described above.
`./ytm-dlp.sh https://music.youtube.com/playlist?list=OLAK5uy_klopEznGorWswD7pRSpS-Ov5aXsffjyGM&si=TCkzzGKGdG6O4ZAg mp3`

This will output the downloaded album to: `~/Music/Tom Morello - The Atlas Underground Fire`

Within that folder will be all the tracks, sorted by track number:
```
01 - Harlem Hellfighter.mp3
02 - Highway to Hell (feat. Bruce Springsteen & Eddie Vedder).mp3
03 - Let’s Get The Party Started (feat. Bring Me The Horizon).mp3
04 - Driving to Texas (feat. Phantogram).mp3
05 - The War Inside (feat. Chris Stapleton).mp3
06 - Hold The Line (feat. grandson).mp3
07 - Naraka (feat. Mike Posner).mp3
08 - The Achilles List (feat. Damian Marley).mp3
09 - Night Witch (feat. phem).mp3
10 - Charmed I’m Sure (feat. Protohype).mp3
11 - Save Our Souls (feat. Dennis Lyxzén of Refused).mp3
12 - On The Shore Of Eternity (feat. Sama’ Abdulhadi).mp3
```

NOTES: 
1) I found that using `<channel>` worked better than `<artist>` or `<album_artist>` due to the way YT Music handles metadata. 
2) If you specify the audio format of "best" ths will result in opus files where available. 
3) You can place this script in your PATH, such as `~/bin` to easily run it from any location on your device.